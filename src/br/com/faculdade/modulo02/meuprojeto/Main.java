package br.com.faculdade.modulo02.meuprojeto;

public class Main {
    public static void main(String[] args) {
        Aluno aluno1 = new Aluno("Giovane", "Dom Casmurro - Machado de Assis", "giovane@gmail.com", "Gravataí-RS");

        aluno1.fazerDownloadLivro();
        aluno1.mostrarLivroBaixado();
    }

}
