package br.com.faculdade.modulo02.meuprojeto;

import java.time.LocalDate;
import java.time.LocalTime;

public class Aluno {
    // atributos
    private String nomeAluno;
    private String nomeLivro;
    private String email;
    private String cidade;

    // construtor
    public Aluno(String nomeAluno, String nomeLivro, String email, String cidade) {
        this.nomeAluno = nomeAluno;
        this.nomeLivro = nomeLivro;
        this.email = email;
        this.cidade = cidade;
    }

    // metodo para fazer o download do livro escolhido
    public void fazerDownloadLivro(){
        System.out.println("\nFazendo download do livro: "+ this.nomeLivro +" . . .\n");
    }

    // metodo para mostrar o livro baixado contendo informacoes adicionais (import das classes LocalDate e LocalTime)
    public void mostrarLivroBaixado(){
        System.out.println("Livro "+ this.nomeLivro +" baixado. Boa leitura!\n");
        System.out.println("Dia do download: "+ LocalDate.now());
        System.out.println("Término do download: "+ LocalTime.now());
    }

    // getters e setters
    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public String getNomeLivro() {
        return nomeLivro;
    }

    public void setNomeLivro(String nomeLivro) {
        this.nomeLivro = nomeLivro;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

}
