package br.com.faculdade.modulo02.colega02;

import java.util.Scanner;

public class Aluno extends Livro {
    // atributos
    private String nomeAluno;
    private String cpf;
    private String email;
    private Livro livro;

    // construtor
    public Aluno(String nome, String cpf, String email) {
        this.nomeAluno = nome;
        this.cpf = cpf;
        this.email = email;
    }

    // getters e setters
    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    // metodo para conferir término de leitura
    public void confereTerminoLeitura(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Se você terminou a leitura do livro digíte (1) para sim ou (2) para não: ");
        int leitura = scan.nextInt();
        if(leitura == 1){
            System.out.println("Leitura do livro concluída. Obrigado, "+ getNomeAluno() +"!");
        } else{
            System.out.println("Leitura do livro não concluída.");
        }
    }

    // metodo para exibir dados do aluno
    public void exibeDadosAluno(){
        System.out.println("-----------------------------");
        System.out.println("Dados do estudante: \n");
        System.out.println("Nome: " + this.getNomeAluno());
        System.out.println("CPF: " + this.getCpf());
        System.out.println("E-mail: " + this.getEmail());
        System.out.println("Livro emprestado: "+ this.getLivro().getNomeLivro());
    }

}
