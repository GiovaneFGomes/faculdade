package br.com.faculdade.modulo02.colega02;

public class Main {
    public static void main(String[] args) {
        Aluno aluno1 = new Aluno("Giovane", "625-363-272-12", "giovane@gmail.com");
        Livro livroAluno1 = new Livro("O cortiço", "Aluísio Azevedo", "978-3-16-148410-0");

        aluno1.setLivro(livroAluno1);

        aluno1.exibeDadosAluno();
        livroAluno1.exibeDadosLivro();

        aluno1.confereTerminoLeitura();

    }
}
