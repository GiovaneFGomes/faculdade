package br.com.faculdade.modulo02.colega02;

public class Livro {
    // atributos
    private String nomeLivro;
    private String autor;
    private String codigo;

    // construtor
    public Livro(String nome, String autor, String codigo) {
        this.nomeLivro = nome;
        this.autor = autor;
        this.codigo = codigo;
    }

    // construtor vazio, já que aluno extends livro
    public Livro(){
    }

    // getters e setters
    public String getNomeLivro() {
        return nomeLivro;
    }

    public void setNomeLivro(String nomeLivro) {
        this.nomeLivro = nomeLivro;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    // metodo que exibe dados do livro
    public void exibeDadosLivro(){
        System.out.println("-----------------------------");
        System.out.println("Dados do livro: \n");
        System.out.println("Nome: " + this.getNomeLivro());
        System.out.println("Autor: " + this.getAutor());
        System.out.println("Código: " + this.getCodigo());
        System.out.println("-----------------------------");
    }

}
