package br.com.faculdade.modulo02.colega01;

public class Professor {
    private String nome;
    private String materia;
    private String email;
    private String cpf;

    public Professor(String nome, String materia, String email, String cpf) {
        this.nome = nome;
        this.materia = materia;
        this.email = email;
        this.cpf = cpf;
    }

    public void gravarAula(){
        System.out.println("Olá Professor " + this.nome + ", aula de " + this.materia + " gravada com sucesso!");
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
