package br.com.faculdade.modulo01;

public class Panvel extends Teclado {

    public static void main(String[] args) {
        System.out.println("----------------------------------------------------------");
        System.out.println("Olá, vamos fazer o seu primeiro cadastro no site da Panvel.");
        System.out.println("----------------------------------------------------------");

        String nome = Teclado.leString("Nome: ");
        String genero = Teclado.leString("Gênero: ");
        int idade = Teclado.leInt("Idade: ");
        String cpf = Teclado.leString("CPF: ");
        String rg = Teclado.leString("RG: ");
        String pais = Teclado.leString("País: ");
        String estado = Teclado.leString("Estado: ");
        String cidade = Teclado.leString("Cidade: ");
        String email = Teclado.leString("E-mail: ");
        String telefone = Teclado.leString("Telefone: ");

        System.out.println("----------------------------------------------------------");
        System.out.println("Cadastro concluído.");
        System.out.println("----------------------------------------------------------");

        System.out.println("Nome: "+ nome);
        System.out.println("Gênero: "+ genero);
        System.out.println("Idade: "+ idade);
        System.out.println("CPF: "+ cpf);
        System.out.println("RG: "+ rg);
        System.out.println("País: "+ pais);
        System.out.println("Estado: "+ estado);
        System.out.println("Cidade: "+ cidade);
        System.out.println("E-mail: "+ email);
        System.out.println("Telefone: "+ telefone);

        System.out.println("----------------------------------------------------------");
    }
}
