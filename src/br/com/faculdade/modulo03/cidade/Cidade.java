package br.com.faculdade.modulo03.cidade;

public class Cidade {

    // atributos
    public int codigo;
    public String descricao;
    public String UF;
    public int qtdeEstudantes;

    // construtor
    public Cidade(int codigo, String descricao, String UF) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.UF = UF;
    }

    // getters e setters
    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getUF() {
        return UF;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }


    public void adicionaNovoEstudante(){
        qtdeEstudantes++;
    }

    // metodo exibeDadosCidade() para as instâncias de Cidade
    public void exibeDadosCidade(){
        System.out.println("------------------------------------------");
        System.out.println("Código: " + getCodigo());
        System.out.println("Descrição: " + getDescricao());
        System.out.println("UF: " + getUF());
        System.out.println("Quantidade de estudantes: " + qtdeEstudantes);
    }

    // utilizando para mostrar o nome da cidade quando o metodo exibeDadosAluno() for chamado
    @Override
    public String toString() {
        return descricao;
    }

}
