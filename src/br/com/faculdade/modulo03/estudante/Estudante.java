package br.com.faculdade.modulo03.estudante;

import br.com.faculdade.modulo03.cidade.Cidade;

import java.util.Scanner;

public class Estudante {

    // atributos
    public int codigo;
    public String nome;
    public String dataNascimento;
    public String email;
    public String senha;
    public Cidade cidade;

    // construtor com o metodo adicionaNovoEstudante()
    public Estudante(int codigo, String nome, String dataNascimento, String email, String senha, Cidade cidade) {
        this.codigo = codigo;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.senha = senha;
        this.cidade = cidade;
        cidade.adicionaNovoEstudante();
    }

    // getters e setters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }


    // metodo que atualiza senha do estudante. Primeiramente ele tem que digitar a senha atual,
    // se a senha digitada for igual a senha atual, ele pode seguir e alterar a senha que, ainda tem um a mais,
    // onde tera uma verificacao para repetir a senha digitada, trazendo assim seguranca ao cliente.
    public void atualizaSenha() {
        Scanner scan = new Scanner(System.in);
        System.out.println("------------------------------------------");
        System.out.print("Antes de alterar sua senha, digite sua senha atual: ");
        String senhaAtual = scan.next();

        if (senhaAtual.equals(getSenha())) {
            System.out.print("Muito bem! Agora digite sua nova senha: ");
            String novaSenha1 = scan.next();

            System.out.print("Digite novamente: ");
            String novaSenha2 = scan.next();

            if (novaSenha1.equals(novaSenha2)) {
                setSenha(novaSenha2);
                System.out.println("------------------------------------------");
                System.out.println("Senha alterada com sucesso!");
            } else {
                System.out.println("------------------------------------------");
                System.out.println("Tente novamente...");
            }

        } else {
            System.out.println("------------------------------------------");
            System.out.println("Tente novamente...");
        }

        System.out.println("------------------------------------------");

    }

    // metodo exibeDadosEstudante() para as instâncias de Estudante
    public void exibeDadosEstudante(){
        System.out.println("------------------------------------------");
        System.out.println("Dados do estudante\n");
        System.out.println("Nome: "+ getNome());
        System.out.println("Data de nascimento: "+ getDataNascimento());
        System.out.println("E-mail: "+ getEmail());
        System.out.println("Código: "+ getCodigo());
        System.out.println("Senha: "+ getSenha());
        System.out.println("Cidade: "+ getCidade());
        System.out.println("------------------------------------------");
    }

}
