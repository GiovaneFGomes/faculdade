package br.com.faculdade.modulo03.main;


import br.com.faculdade.modulo03.cidade.Cidade;
import br.com.faculdade.modulo03.estudante.Estudante;

public class TesteFun4Study {
    public static void main(String[] args) {
        // primeira cidade
        Cidade cidade1 = new Cidade(1, "Rio Grande do Sul", "RS");

        // segunda cidade
        Cidade cidade2 = new Cidade(2, "São Paulo", "SP");

        // estudantes da primeira cidade
        Estudante estudante1 = new Estudante(cidade1.getCodigo(),"Giovane","19/10/2001","giovane@gmail","senha1",cidade1);
        Estudante estudante2 = new Estudante(cidade1.getCodigo(),"Lucas","23/12/2002","lucas@gmail","senha2",cidade1);

        // estudantes da segunda cidade
        Estudante estudante3 = new Estudante(cidade1.getCodigo(),"Ana","11/11/2001","ana@gmail","senha1",cidade2);
        Estudante estudante4 = new Estudante(cidade1.getCodigo(),"Rafaella","23/06/2001","rafaella@gmail","senha2",cidade2);


        cidade1.exibeDadosCidade();

        estudante1.atualizaSenha();
        System.out.println("Sua senha é " + estudante1.getSenha());

        estudante1.exibeDadosEstudante();
    }

}
